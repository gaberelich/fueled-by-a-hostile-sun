# Fueled by a Hostile sun

This Git repository contains the Creative Commons SRDs for "Under a Hostile Sun" entitled "Fueled by a Hostile Sun"

## Tabletop RPG Premise
The settlement’s life support is at the brink.  A solution to this world’s dangerous conditions must be devised & corruption amongst the station leaders must be investigated. But the Void of Space was not meant for Man. These planets are not “Mother” like Earth – you are under a hostile sun.

## See on Itch.io
[https://muckraker.itch.io/under-a-hostile-sun](https://muckraker.itch.io/under-a-hostile-sun)

## Game Design Goals
Game Design Goals:  This RPG . . . 
1.	is rules-lite, yet still a simulation with game depth (as opposed to being a creative writing exercise, which some rules-lite RPGs border on).  
2.	has an elegant resource currency system which every other game mechanic connects to in some way.
3.	Replaces endless lists of spells / equipment with a unified alchemy system.
4.	Allows for creature collecting, without having complicated creature stat blocks.
5.	. . . and it does it while exploring a non-violent yet brutal premise – surviving on strange worlds, in the void of space

